## Kubectl Basics

### Exemplo de deploy do Nginx
sudo kubectl run nginx --image nginx --port 80
sudo kubectl expose deploy/nginx --port 80 --type NodePort 
sudo minikube service nginx --url

Para não ter que digitar sudo o tempo todo execute os comandos:

> sudo cp /etc/kubernetes/admin.conf $HOME/
    sudo chown $(id -u):$(id -g) $HOME/admin.conf
    export KUBECONFIG=$HOME/admin.conf


##### Rodar o MongoDB com o kubectl**

sudo kubectl run mongodb --image mongo:3.5 --port 27017

-> com isso cria a definição de deployment0
-> vai criar definicao de ReplicaSet
-> vaio criar a definicao de Pods


#### Alguns comandos úteis

```
kubectl get pods -> visualizar containers

kubectl get pods -w -> visualizar + watch containers

kubectl logs ID_POD -> visualizar logs

kubectl exec -it ID_POD -- /bin/bash -> acessa o pod via SSH

kubectl create -f nomearquivo.json

kubectl get pods --output wide - para pegar o iP

kubectl expose pod api-heroes --port 4000 --type NodePort => isso gera uma porta "alta" para expor a porta 4000 

minikube service <nome_do_serviço> --url => devolve a url da aplicação

```


<!-- kubectl port-forward api-heroes 4000:4000 -->
kubectl expose pod api-heroes \
    --port 4000 \
    --type NodePort

minikube service api-heroes --url


> para aparecer o dashboard sem problema no redirect (somente na AWS)
sudo kubectl proxy --address='0.0.0.0' \
    --disable-filter=true & 


Modelo de um Template de Pod

{
    "apiVersion":"v1",
    "kind": "Pod",
    "metadata": {
        "name": "api-heroes",
        "labels": {
            "app": "api-heroes",
            "version": "v1"
        }
    },
    "spec": {
        "containers": [
            {
                "name": "api-heroes",
                "image": "expressivecode/api-heroes-mongodb",
                "ports": [{
                    "containerPort": 4000
                }],
                "env": [
                    {
                        "name": "MONGO_URL",
                        "value": "172.17.0.3"
                    },
                    {
                        "name": "PORT",
                        "value": 4000
                    }
                ]
            }
        ]
    }
}
